package log

import (
	"fmt"
	"log"
)

func init() {

	if Logger == nil {
		NewLogEntry("unknown", "unknown", "unknown")
	}
}

type Entry struct {
	Version string
	App     string
	Env     string
	S       string
}

var Logger *Entry

func NewLogEntry(version, appName, env string) {
	Logger = &Entry{
		Version: version,
		App:     appName,
		Env:     env,
	}
	Logger.S = fmt.Sprintf("version: %s, app: %s, env: %s", Logger.Version, Logger.App, Logger.Env)
}

func Info(args ...interface{}) {
	log.Print(append([]interface{}{Logger.S, ", level: info -"}, args...)...)
}

func Infoln(args ...interface{}) {
	log.Println(append([]interface{}{Logger.S, ", level: info -"}, args...)...)
}

func Infof(format string, args ...interface{}) {
	log.Printf(Logger.S+" , level: info - "+format, args...)
}

func Warn(args ...interface{}) {
	log.Print(append([]interface{}{Logger.S, ", level: warn -"}, args...)...)
}

func Warnln(args ...interface{}) {
	log.Println(append([]interface{}{Logger.S, ", level: warn -"}, args...)...)
}

func Warnf(format string, args ...interface{}) {
	log.Printf(Logger.S+" , level: warn - "+format, args)
}

func Error(args ...interface{}) {
	log.Print(append([]interface{}{Logger.S, ", level: error -"}, args...)...)
}

func Errorln(args ...interface{}) {
	log.Println(append([]interface{}{Logger.S, ", level: error -"}, args...)...)
}

func Errorf(format string, args ...interface{}) {
	log.Printf(Logger.S+" , level: error - "+format, args)
}

func Fatal(args ...interface{}) {
	log.Fatal(append([]interface{}{Logger.S, ", level: fatal -"}, args...)...)
}

func Fatalln(args ...interface{}) {
	log.Fatalln(append([]interface{}{Logger.S, ", level: fatal -"}, args...)...)
}

func Fatalf(format string, args ...interface{}) {
	log.Fatalf(Logger.S+" , level: fatal - "+format, args)
}
