package swagger

import (
	"bytes"
	"fmt"
	"html/template"
	"os"
	"strings"

	"github.com/aws/aws-lambda-go/events"
	"github.com/go-openapi/runtime/middleware"
	"github.com/go-openapi/spec"
)

const (
	redocLatest   = "https://cdn.jsdelivr.net/npm/redoc/bundles/redoc.standalone.js"
	redocTemplate = `<!DOCTYPE html>
<html>
  <head>
    <title>{{ .Title }}</title>
		<!-- needed for adaptive design -->
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,700|Roboto:300,400,700" rel="stylesheet">

    <!--
    ReDoc doesn't change outer page styles
    -->
    <style>
      body {
        margin: 0;
        padding: 0;
      }
    </style>
  </head>
  <body>
    <redoc spec-url='{{ .SpecURL }}'></redoc>
    <script src="{{ .RedocURL }}"> </script>
  </body>
</html>
`
)

func RedocTemplate() string {
	return redocTemplate
}

func Redoc(c *middleware.Context, sp *spec.Swagger) (events.APIGatewayProxyResponse, error) {
	var title string
	if sp != nil && sp.Info != nil && sp.Info.Title != "" {
		title = sp.Info.Title
	}
	opts := middleware.RedocOpts{
		BasePath: c.BasePath(),
		Title:    title,
	}
	opts.EnsureDefaults()

	//pth := path.Join(opts.BasePath, opts.Path)
	tmpl := template.Must(template.New("redoc").Parse(RedocTemplate()))

	buf := bytes.NewBuffer(nil)
	opts.SpecURL = os.Getenv("REDOC_PREFIX") + "/swagger.json"
	_ = tmpl.Execute(buf, opts)
	b := buf.Bytes()

	headers := make(map[string]string)
	headers["Content-Type"] = "text/html"
	return events.APIGatewayProxyResponse{
		Headers:    headers,
		Body:       string(b),
		StatusCode: 200}, nil
}

func SwaggerJson(b []byte) (events.APIGatewayProxyResponse, error) {
	host := os.Getenv("HOST")
	basePath := os.Getenv("BASE_PATH")
	body := strings.ReplaceAll(string(b), "\"host\": \"api.bsgroup.io\"", fmt.Sprintf("\"host\": \"%s\"", host))
	body = strings.ReplaceAll(body, "\"basePath\": \"/dev\"", fmt.Sprintf("\"basePath\": \"/%s\"", basePath))
	headers := make(map[string]string)
	headers["Content-Type"] = "application/json"
	headers["Access-Control-Allow-Origin"] = "*"
	headers["Access-Control-Allow-Methods"] = "*"
	return events.APIGatewayProxyResponse{
		Headers:    headers,
		Body:       body,
		StatusCode: 200}, nil
}
