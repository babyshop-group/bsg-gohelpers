// go generate
package main

import (
	"bytes"
	"io/ioutil"
	"log"
	"os"
	"regexp"
	"strings"
	"text/template"
)

var rootfolder = "tmp"

func main() {
	funcname := "dummy"
	cmdMaingo := getTemplate(cmdMain, funcname)
	cmdMainTestgo := getTemplate(cmdMainTest, funcname)
	pkgFuncHandlergo := getTemplate(pkgFuncHandler, funcname)
	pkgFuncWrappergo := getTemplate(pkgFuncWrapper, funcname)

	gomodgo := getTemplate(gomod, funcname)
	serverlessyamlgo := getTemplate(serverlessyaml, funcname)

	makefilego := getTemplate(makefile, funcname)
	swaggeryamlgo := getTemplate(swaggeryaml, funcname)

	os.MkdirAll("tmp/cmd", os.ModePerm)
	os.MkdirAll("tmp/pkg/"+funcname, os.ModePerm)

	err := ioutil.WriteFile(rootfolder+"/cmd/main.go", []byte(cmdMaingo), 0644)
	fatallnErr(err)
	err = ioutil.WriteFile(rootfolder+"/cmd/main_test.go", []byte(cmdMainTestgo), 0644)
	fatallnErr(err)
	err = ioutil.WriteFile(rootfolder+"/pkg/"+funcname+"/handler.go", []byte(pkgFuncHandlergo), 0644)
	fatallnErr(err)
	err = ioutil.WriteFile(rootfolder+"/pkg/"+funcname+"/wrapper.go", []byte(pkgFuncWrappergo), 0644)
	fatallnErr(err)
	err = ioutil.WriteFile(rootfolder+"/go.mod", []byte(gomodgo), 0644)
	fatallnErr(err)
	err = ioutil.WriteFile(rootfolder+"/serverless.yaml", []byte(serverlessyamlgo), 0644)
	fatallnErr(err)
	err = ioutil.WriteFile(rootfolder+"/Makefile", []byte(makefilego), 0644)
	fatallnErr(err)
	err = ioutil.WriteFile(rootfolder+"/swagger.yaml", []byte(swaggeryamlgo), 0644)
	fatallnErr(err)

}

func fatallnErr(err error) {
	if err != nil {
		log.Fatalln(err)
	}
}

func getTemplate(tempStr, funcname string) string {
	var tpl bytes.Buffer
	t := template.
		Must(template.New("").Parse(tempStr))
	if err := t.Execute(&tpl, struct {
		FolderName                         string
		SlsFunctionName                    string
		SlsFunctionNameCaptilizedCamelcase string
		SlsFunctionTagName                 string
		SlsFunctionTagNameCapitalized      string
		SlsFunctionNameCapitalized         string
	}{
		FolderName:                         rootfolder,
		SlsFunctionName:                    funcname,
		SlsFunctionNameCaptilizedCamelcase: ToCamel(funcname),
		SlsFunctionNameCapitalized:         strings.Title(strings.ToLower(funcname)),
		SlsFunctionTagName:                 funcname,
		SlsFunctionTagNameCapitalized:      strings.Title(strings.ToLower(funcname)),
	}); err != nil {
		log.Fatalln(err)
	}
	return tpl.String()
}

var cmdMain = `package main

import (
  "os"

  "bitbucket.org/babyshop-group/bsg-serverless/{{ .FolderName }}/pkg/{{ .SlsFunctionName }}"
  "github.com/aws/aws-lambda-go/lambda"
  "github.com/go-openapi/runtime/middleware"
)

func main() {
  middleware.Debug = true
  wrapper := {{ .SlsFunctionName }}.New{{ .SlsFunctionNameCaptilizedCamelcase }}Wrapper()
  switch os.Getenv("SLS_TARGET") {
  case "redoc":
    lambda.Start(wrapper.RedocHandle)
  case "swaggerjson":
    lambda.Start(wrapper.SwaggerJsonHandle)
  default:
    lambda.Start(wrapper.{{ .SlsFunctionNameCaptilizedCamelcase }}Handle)
  }
}`

var cmdMainTest = `package main

import "testing"

func TestMain(t *testing.T) {
  var err error

  if err != nil {
    t.Fatal("expect no err but got", err)
  }
}`

var pkgFuncHandler = `package {{ .SlsFunctionName }}

import (
  "bitbucket.org/babyshop-group/bsg-serverless/{{ .FolderName }}/pkg/{{ .SlsFunctionName }}/gen/models"
  "bitbucket.org/babyshop-group/bsg-serverless/{{ .FolderName }}/pkg/{{ .SlsFunctionName }}/gen/restapi/operations/{{ .SlsFunctionTagName }}"
  "github.com/go-openapi/runtime/middleware"
)

type Handler struct {
  CurrentError error
}

func (handler *Handler) Handle(params {{ .SlsFunctionTagName }}.{{ .SlsFunctionNameCaptilizedCamelcase }}Params) middleware.Responder {
  if handler.CurrentError != nil {
    errString := handler.CurrentError.Error()
    return {{ .SlsFunctionTagName }}.New{{ .SlsFunctionNameCaptilizedCamelcase }}BadRequest().WithPayload(&models.{{ .SlsFunctionNameCaptilizedCamelcase }}Response{&errString})

  }

  // your handler implementation herer

  return {{ .SlsFunctionName }}.New{{ .SlsFunctionNameCaptilizedCamelcase }}OK()
}
`

var pkgFuncWrapper = `package {{ .SlsFunctionName }}

import (
  "encoding/json"
  "log"

  "bitbucket.org/babyshop-group/bsg-serverless/{{ .FolderName }}/pkg/{{ .SlsFunctionName }}/gen/restapi"
  "bitbucket.org/babyshop-group/bsg-serverless/{{ .FolderName }}/pkg/{{ .SlsFunctionName }}/gen/restapi/operations"
  "bitbucket.org/babyshop-group/bsg-serverless/{{ .FolderName }}/pkg/{{ .SlsFunctionName }}/gen/restapi/operations/{{ .SlsFunctionTagName }}"
  "github.com/aws/aws-lambda-go/events"
  "github.com/go-openapi/loads"
  "github.com/go-openapi/runtime/middleware"

  swagger "bitbucket.org/babyshop-group/bsg-gohelpers/pkg/swagger"
)

type {{ .SlsFunctionNameCaptilizedCamelcase }}Wrapper struct {
  {{ .SlsFunctionNameCaptilizedCamelcase }} *{{ .SlsFunctionTagName }}.{{ .SlsFunctionNameCaptilizedCamelcase }}
  Spec        *loads.Document
}

func New{{ .SlsFunctionNameCaptilizedCamelcase }}Wrapper() *{{ .SlsFunctionNameCaptilizedCamelcase }}Wrapper {
  swaggerSpec, err := loads.Embedded(restapi.SwaggerJSON, restapi.FlatSwaggerJSON)
  if err != nil {
    log.Fatalln(err)
  }
  {{ .SlsFunctionTagName }}Api := operations.New{{ .SlsFunctionTagNameCapitalized }}API(swaggerSpec)

  {{ .SlsFunctionTagName }}Api.Init()
  context := middleware.NewRoutableContext(swaggerSpec, {{ .SlsFunctionTagName }}Api, middleware.DefaultRouter(swaggerSpec, {{ .SlsFunctionTagName }}Api))
  service := {{ .SlsFunctionTagName }}.New{{ .SlsFunctionNameCaptilizedCamelcase }}(context, &Handler{})
  return &{{ .SlsFunctionNameCaptilizedCamelcase }}Wrapper{service, swaggerSpec}
}

func (h *{{ .SlsFunctionNameCaptilizedCamelcase }}Wrapper) SwaggerJsonHandle(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
  b := h.Spec.Raw()
  return swagger.SwaggerJson(b)

}

func (h *{{ .SlsFunctionNameCaptilizedCamelcase }}Wrapper) RedocHandle(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
  c := h.{{ .SlsFunctionNameCaptilizedCamelcase }}.Context

  sp := h.Spec.Spec()
  return swagger.Redoc(c, sp)
}

func (h *{{ .SlsFunctionNameCaptilizedCamelcase }}Wrapper) {{ .SlsFunctionNameCaptilizedCamelcase }}Handle(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
  params, err := h.Validate(request)
  if err != nil {
    handler := h.{{ .SlsFunctionNameCaptilizedCamelcase }}.Handler.(*Handler)
    handler.CurrentError = err
  }

  result := h.{{ .SlsFunctionNameCaptilizedCamelcase }}.Handler.Handle(*params)

  bodyPayload, err := json.Marshal(result)
  if err != nil {
    return events.APIGatewayProxyResponse{StatusCode: GetStatusCode(nil)}, err
  }

  return events.APIGatewayProxyResponse{Body: string(bodyPayload), StatusCode: GetStatusCode(result)}, nil
}

func (h *{{ .SlsFunctionNameCaptilizedCamelcase }}Wrapper) Validate(request events.APIGatewayProxyRequest) (*{{ .SlsFunctionTagName }}.{{ .SlsFunctionNameCaptilizedCamelcase }}Params, error) {
  r := &swagger.RequestAccessor{}
  req, err := r.EventToRequest(request)
  if err != nil {
    return nil, err

  }
  route, rCtx, _ := h.{{ .SlsFunctionNameCaptilizedCamelcase }}.Context.RouteInfo(req)
  if rCtx != nil {
    req = rCtx
  }

  var params = {{ .SlsFunctionTagName }}.New{{ .SlsFunctionNameCaptilizedCamelcase }}Params()

  if err := h.{{ .SlsFunctionNameCaptilizedCamelcase }}.Context.BindValidRequest(req, route, &params); err != nil { // bind params
    return nil, err
  }
  return &params, err
}
func GetStatusCode(resp interface{}) int {
  if resp == nil {
    return 500
  }

  switch resp.(type) {
  case *{{ .SlsFunctionTagName }}.{{ .SlsFunctionNameCaptilizedCamelcase }}OK:
    return 200
  case *{{ .SlsFunctionTagName }}.{{ .SlsFunctionNameCaptilizedCamelcase }}BadRequest:
    return 400
  default:
    log.Println("unknown response type")
    return 400
  }
}`

var gomod = `module bitbucket.org/babyshop-group/bsg-serverless/{{ .FolderName }}

go 1.14

require (
  bitbucket.org/babyshop-group/bsg-gohelpers v0.0.3
  cloud.google.com/go/bigquery v1.8.0
  github.com/asaskevich/govalidator v0.0.0-20200428143746-21a406dcc535 // indirect
  github.com/aws/aws-lambda-go v1.17.0
  github.com/go-openapi/errors v0.19.4
  github.com/go-openapi/loads v0.19.5
  github.com/go-openapi/runtime v0.19.15
  github.com/go-openapi/spec v0.19.8
  github.com/go-openapi/strfmt v0.19.5
  github.com/go-openapi/swag v0.19.9
  github.com/go-openapi/validate v0.19.8
  github.com/jessevdk/go-flags v1.4.0
  github.com/kr/text v0.2.0 // indirect
  github.com/mitchellh/mapstructure v1.3.2 // indirect
  github.com/niemeyer/pretty v0.0.0-20200227124842-a10e7caefd8e // indirect
  github.com/stretchr/testify v1.6.1 // indirect
  go.mongodb.org/mongo-driver v1.3.4 // indirect
  golang.org/x/net v0.0.0-20200602114024-627f9648deb9
  google.golang.org/api v0.27.0
  gopkg.in/check.v1 v1.0.0-20200227125254-8fa46927fb4f // indirect
  gopkg.in/yaml.v2 v2.3.0 // indirect
  gopkg.in/yaml.v3 v3.0.0-20200615113413-eeeca48fe776 // indirect
)`

var serverlessyaml = `service: {{ .FolderName }}
frameworkVersion: '>=1.28.0 <2.0.0'
provider:
  name: aws
  runtime: go1.x
  package:
  exclude:
    - ./**
  include:
    - ./build/**
functions:
  redoc:
    handler: build/{{ .SlsFunctionTagName }}
    environment:
      SLS_TARGET: redoc
      REDOC_PREFIX: "/${self:provider.stage}"
    events:
      - http:
          path: doc
          method: get
  swaggerjson:
    handler: build/{{ .SlsFunctionTagName }}
    environment:
      SLS_TARGET: swaggerjson
      #BASE_PATH: "${self:provider.stage}"
      HOST: "${self:custom.${self:provider.stage}.HOST}/${self:provider.stage}"
    events:
      - http:
          path: swagger.json
          method: get
  {{ .SlsFunctionTagName }}:
    handler: build/{{ .SlsFunctionTagName }}
    events:
      - http:
          path: {{ .SlsFunctionTagName }}
          method: post
custom:
  dev:
    HOST: myhost.io #7pwb4a4ueb.execute-api.us-east-1.amazonaws.com
`
var makefile = `.PHONY: build clean deploy

fix-dep:
	go get github.com/go-openapi/errors@v0.19.4

gen-api-server:
	rm -rf pkg/{{ .SlsFunctionName }}/gen/ && mkdir -p pkg/{{ .SlsFunctionName }}/gen/
	swagger generate server -f swagger.yaml   -A{{ .SlsFunctionName }}  -t pkg/{{ .SlsFunctionName }}/gen/


build:
	go mod vendor
	env GOOS=linux go build -ldflags="-s -w" -o build/{{ .SlsFunctionName }} cmd/main.go

clean:
	rm -rf ./bin

deploy: clean build
	sls deploy --verbose`

var swaggeryaml = `definitions:
  {{ .SlsFunctionName }}Body:
    properties:
      name:
        type: string
        description: some description of name here
  {{ .SlsFunctionName }}Response:
    description: Response of {{ .SlsFunctionName }}
    properties:
      error:
        type: string
        description: BSG errors in string
        x-nullable: true
    type: object
host: api.bsgroup.io
basePath: "/"
info:
  title: Babyshop {{ .SlsFunctionNameCapitalized }} API
  version: '1.0'
paths:
  /{{ .SlsFunctionName }}:
    post:
      description: create extractor jobs on the datasets and tables
      consumes:
        - application/json
      operationId: {{ .SlsFunctionNameCapitalized }}
      parameters:
        - in: body
          name: body
          required: true
          schema:
            $ref: '#/definitions/{{ .SlsFunctionName }}Body'
      produces:
        - application/json
      responses:
        '400':
          description: "Bad Params"
          schema:
            $ref: '#/definitions/{{ .SlsFunctionName }}Response'
        '200':
          description: Success
        '500':
          description: Internal Server Error
      tags:
        - {{ .SlsFunctionTagName }}
schemes:
- https
swagger: '2.0'
x-components: {}
`

// Converts a string to CamelCase
func toCamelInitCase(s string, initCase bool) string {
	s = addWordBoundariesToNumbers(s)
	s = strings.Trim(s, " ")
	n := ""
	capNext := initCase
	for _, v := range s {
		if v >= 'A' && v <= 'Z' {
			n += string(v)
		}
		if v >= '0' && v <= '9' {
			n += string(v)
		}
		if v >= 'a' && v <= 'z' {
			if capNext {
				n += strings.ToUpper(string(v))
			} else {
				n += string(v)
			}
		}
		if v == '_' || v == ' ' || v == '-' || v == '.' {
			capNext = true
		} else {
			capNext = false
		}
	}
	return n
}

// ToCamel converts a string to CamelCase
func ToCamel(s string) string {
	if uppercaseAcronym[s] {
		s = strings.ToLower(s)
	}
	return toCamelInitCase(s, true)
}

var numberSequence = regexp.MustCompile(`([a-zA-Z])(\d+)([a-zA-Z]?)`)
var numberReplacement = []byte(`$1 $2 $3`)

func addWordBoundariesToNumbers(s string) string {
	b := []byte(s)
	b = numberSequence.ReplaceAll(b, numberReplacement)
	return string(b)
}

var uppercaseAcronym = map[string]bool{
	"ID": true,
}
